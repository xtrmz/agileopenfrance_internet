# Internet libre pour les athénien-e-s

L'objectif est de décrire comment fournir une connexion wifi aux participants de l'agile open france.

## Matériel

- clef 4g
- routeur
- 4 APs (1 retro louge, 1 accueil, 1 pleinière, 1 gymnase, 0 spa)
- cables

## Salle Rétro-lounge
### Mise en place du raccordement internet

C'est par les antennes 4G et les cartes sims : bouyges, orange, free qu'internet sera disponible.
Les antennes sont collées à la vitre en direction de chez Schwartz.
La position des antennes a été déterminé en fonction de la localisation des [pylônes des opérateurs](https://www.couverture-mobile.fr/#lat=4836265&lng=744390&z=15).

```mermaid
graph TD;
  
Orange --> routeur
Bouygues --> routeur
Free --> routeur
routeur --> switch
switch --> rallonge_de_20m
rallonge_de_20m --> lan_arnold
switch --> antenne_wifi_ruckus_wireless
switch --> controlleur_wifi
```

Le routeur est un modèle du [type](https://www.netgate.com/solutions/pfsense/sg-4860.html).


Internet est maintenant disponible en wifi autour du rétro lounge.

Le lan arnold est une prise ethernet branché au premier étage à droite (à coté de l'escalier) et raccordé par une prise de 20 mètres jusqu'aux équipements réseaux du retro-lounge.

## Accueil
On débranche le lan_arnold (wan sur le routeur arnold) afin d'éviter les conflits dhcp (2 serveurs sur le même réseau).
Le ```lan_arnold``` est branché sur un ```switch```.

```mermaid
graph TD;

lan_arnold --> switch
switch --> antenne_wifi_ruckus_wireless
```

## Salle de brassage de l'extension (couloir du spa)

On brasse la prise B14 sur le panel sur la deuxième rangée à partir du haut.

## Salle des marchés
Depuis la prise B14 de la salle des marchés, je branche une ```antenne_wifi```
Cela permet de proposer le wifi dans cette partie de l'hotel.
```mermaid
graph TD;

lan_arnold --> antenne_wifi
```